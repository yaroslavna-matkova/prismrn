const process = require('process');
const child_process = require('child_process');
const path = require('path');

console.log(`This platform is ${process.platform}`);

var rmcmd = 'rm ';
var rmrfcmd = 'rm -rf ';
if (process.platform === 'win32') {
    rmrfcmd = 'rmdir /s /q ';
    rmcmd = 'del ';
}

var filename = path.resolve(__dirname, 'node_modules/react-native/local-cli/core/__fixtures__/files/package.json');
console.log(`Will remove file ${filename}`);
child_process.exec(rmcmd + filename)