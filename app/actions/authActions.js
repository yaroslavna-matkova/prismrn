import * as types from '../constants/actionTypes';

export function login(userInfo) {
    return {
        type: types.AUTH_LOGIN,
        user: userInfo
    };
}

export function logout() {
    return {
        type: types.AUTH_LOGOUT
    };
}