import * as types from '../constants/actionTypes';

export function loadingStart() {
    return {
        type: types.ROUTE_LOADING_START
    };
}

export function loadingEnd() {
    return {
        type: types.ROUTE_LOADING_END
    };
}

export function loginSuccess() {
    return {
        type: types.ROUTE_LOGIN_SUCCESS
    };
}

export function loginFail() {
    return {
        type: types.ROUTE_LOGIN_FAIL
    };
}

export function logout() {
    return {
        type: types.ROUTE_LOGOUT
    };
}

export function home() {
    return {
        type: types.ROUTE_HOME
    };
}

export function afterWelcome() {
    return {
        type: types.ROUTE_TABBED_SCREEN
    };
}

export function people() {
    return {
        type: types.ROUTE_PEOPLE
    };
}

export function team() {
    return {
        type: types.ROUTE_TEAM
    };
}