import { NavigationActions } from 'react-navigation';

export function setParamsAction(parameters = {}, routeKey) {
    return NavigationActions.setParams({
        params: parameters,
        key: routeKey,
    });
}