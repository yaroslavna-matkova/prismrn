import * as types from '../constants/actionTypes';

export function all(employees) {
    return {
        type: types.EMPLOYEES_ALL,
        list: employees
    };
}

export function filter(filteringString) {
    return {
        type: types.EMPLOYEES_FILTER,
        filter: filteringString
    };
}

export function addToTeam(id) {
    return {
        type: types.EMPLOYEES_ADD,
        userId: id
    };
}

export function removeFromTeam(id) {
    return {
        type: types.EMPLOYEES_REMOVE,
        userId: id
    };
}