import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, Text, Button, StyleSheet } from 'react-native';

import { loadingStart, afterWelcome } from '../actions/routeActions';
import { all as gotEmployees } from '../actions/employeesActions';
import { strings } from '../constants/localization'
import { allEmployees as loadEmployees } from '../services/prismRequests'

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center'
	},
	title: {
		fontSize: 24,
		marginBottom: 25,
		marginHorizontal: 15,
		alignSelf: 'center',
		textAlign: 'center'
	}
});

class HomeScreen extends React.Component {
	static propTypes = {
		navigation: PropTypes.object.isRequired,
		user: PropTypes.object.isRequired,
	};

	static navigationOptions = {
		title: strings.homeScreenTitle
	};

	render() {
		const { navigation, user, onLoaded } = this.props;
		return (
			<View style={styles.container}>
				<Text style={styles.title}>Hello, {user.FirstName} {user.LastName}!</Text>
				<Button
					title={strings.continue}
					onPress={() => {
						navigation.dispatch(loadingStart());
						onFinish = (employees) => {
							onLoaded(employees);
							navigation.dispatch(afterWelcome());
						};
						loadEmployees(onFinish, onFinish);
					}}
				/>
			</View>
		);
	}
}

const mapStateToProps = state => ({
	user: state.auth.user,
});

const mapDispatchToProps = dispatch => ({
	onLoaded: (employees) => dispatch(gotEmployees(employees))
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);