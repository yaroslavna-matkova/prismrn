import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

import { strings } from '../constants/localization';
import TeamList from '../components/TeamList';
import { setParamsAction } from '../actions/navigatorActions';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center'
	},
	message: {
		fontSize: 24,
		marginHorizontal: 15,
		alignSelf: 'center',
		textAlign: 'center'
	}
});

class TeamScreen extends React.Component {
	static propTypes = {
		teamLength: PropTypes.number.isRequired,
	}

	static navigationOptions = ({ navigation }) => ({
		tabBarIcon: ({ focused, tintColor }) => { return <Icon name='user-following' size={20} color={tintColor} />; },
		title: `${strings.teamScreenTitle} (${navigation.state.params ? navigation.state.params.count : 0})`
	})

	componentWillReceiveProps(nextProps) {
		if (nextProps.teamLength !== this.props.teamLength) {
			const { setParams } = this.props;
			setParams({ count: nextProps.teamLength }, 'Team');
		}
	}

	render() {
		const { teamLength } = this.props;
		return (
			<View style={styles.container}>
				{teamLength > 0 ? <TeamList /> : <Text style={styles.message}>{strings.teamEmpty}</Text>}
			</View>
		);
	}
}

const mapStateToProps = state => ({
	teamLength: state.employees.team && state.employees.team.length
});

const mapDispatchToProps = dispatch => ({
	setParams: (params, route) => dispatch(setParamsAction(params, route))
});

export default connect(mapStateToProps, mapDispatchToProps)(TeamScreen);