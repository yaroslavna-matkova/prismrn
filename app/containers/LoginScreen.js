import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { View, Text, TextInput, Button, StyleSheet, ActivityIndicator, Keyboard } from 'react-native';

import { loadingStart, loginSuccess, loginFail } from '../actions/routeActions';
import { login } from '../actions/authActions';
import { signin } from '../services/prismRequests'
import { strings } from '../constants/localization';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center'
	},
	title: {
		fontSize: 16,
		alignSelf: 'center'
	}
});

class LoginScreen extends React.Component {
	static propTypes = {
		navigation: PropTypes.object.isRequired,
		onLoginSuccess: PropTypes.func.isRequired
	};

	static navigationOptions = {
		header: null,
	};

	credentials = {
		username: '',
		password: ''
	}

	render() {
		const { navigation, onLoginSuccess, isLoading } = this.props;
		return (
			<View style={styles.container}>
				{isLoading && <ActivityIndicator size="large" />}
				{!isLoading && <View>
					<Text style={styles.title}>{strings.loginTitle}</Text>
					<TextInput
						placeholder={strings.username}
						onChangeText={(text) => this.credentials.username = text} />
					<TextInput
						placeholder={strings.password}
						secureTextEntry={true}
						onChangeText={(text) => this.credentials.password = text} />
					<Button
						title={strings.login}
						onPress={() => {
							navigation.dispatch(loadingStart());
							Keyboard.dismiss();
							signin(this.credentials,
								(response) => {
									onLoginSuccess(response);
									navigation.dispatch(loginSuccess());
								},
								(error) => {
									console.log(error);
									navigation.dispatch(loginFail());
								})
						}}
					/>
				</View>}
			</View>
		);
	}
}

const mapStateToProps = state => ({
	isLoading: state.navigation.isLoading,
});

const mapDispatchToProps = dispatch => ({
	onLoginSuccess: (user) => dispatch(login(user))
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);