import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

import { strings } from '../constants/localization';
import { filter } from '../actions/employeesActions'
import EmployeesList from '../components/EmployeesList';
import FilterBar from '../components/FilterBar';

const PeopleScreen = ({ onFilter }) => (
	<View style={{ marginBottom: 60 }}>
		<FilterBar onTextChanged={(text) => onFilter(text)} />
		<EmployeesList />
	</View>
);

PeopleScreen.navigationOptions = {
	title: strings.peopleScreenTitle,
	tabBarIcon: ({ focused, tintColor }) => { return <Icon name={'people'} size={20} color={tintColor} />; }	
};

PeopleScreen.propTypes = {
	onFilter: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
	onFilter: (text) => { dispatch(filter(text)) }
});

export default connect(null, mapDispatchToProps)(PeopleScreen);