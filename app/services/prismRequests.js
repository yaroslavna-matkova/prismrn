const URL = 'https://prism.akvelon.net:8441/';

function request(path, onSuccess = null, onFail = null, options = {}) {
    fetch(URL + path, {
        ...options,
        credentials: 'include'
    })
        .then(response => {
            if (!response.ok) return Promise.reject(response._bodyInit);
            return response.json();
        })
        .then(responseJson => {
            if (onSuccess)
                onSuccess(responseJson);
        })
        .catch(onFail);
}

export function signin(credentials, onSuccess, onFail) {
    var token = "Basic " + Buffer.from(JSON.stringify({ ...credentials, 'persistent': true })).toString('base64');

    request('api/system/signin', () => info(onSuccess), onFail, {
        headers: {
            'Authorization': token,
        }
    });
}

export function info(onSuccess, onFail) {
    request('api/system/loggedinfo', onSuccess, onFail);
}

export function signout() {
    request('api/system/signout');
}

export function allEmployees(onSuccess, onFail) {
    request('api/employees/all', onSuccess, onFail);
}

export function photoUrl(id) {
    return URL + 'api/system/getphoto/' + id;
}