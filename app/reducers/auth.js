import * as types from '../constants/actionTypes';

const initialAuthState = { isLoggedIn: false, user: {} };

export function auth(state = initialAuthState, action) {
	switch (action.type) {
		case types.AUTH_LOGIN:
			return { ...state, isLoggedIn: true, user: action.user };
		case types.AUTH_LOGOUT:
			return { ...state, isLoggedIn: false, user: {} };
		default:
			return state;
	}
}