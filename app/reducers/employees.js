import * as types from '../constants/actionTypes';

const initialState = { list: [], filteredList: [], team: [], filter: '' };
var employeesList = [];

var find = (string, substring) => string && string.toLowerCase().indexOf(substring.toLowerCase()) >= 0;
var filter = (i, text) => find(i.LastName, text) || find(i.FirstName, text) || find(i.Mail, text);
var comparator = (a, b) => a.LastName.localeCompare(b.LastName) || a.FirstName.localeCompare(b.FirstName);

export function employees(state = initialState, action) {
    let newTeam;
    switch (action.type) {
        case types.EMPLOYEES_ALL:
            employeesList = action.list.sort(comparator);
            return { ...state, list: employeesList, filteredList: employeesList };
        case types.EMPLOYEES_FILTER:
            let newList = action.filter === '' ? Array.from(employeesList) : employeesList.filter((i) => filter(i, action.filter));
            return { ...state, filteredList: newList, filter: action.filter };
        case types.EMPLOYEES_ADD:
            newTeam = Array.from(state.team);
            newTeam.push(action.userId)
            return { ...state, team: newTeam };
        case types.EMPLOYEES_REMOVE:
            newTeam = Array.from(state.team);
            let index = state.team.indexOf(action.userId);
            if (index !== -1)
                newTeam.splice(index, 1);
            return { ...state, team: newTeam };

        //Clean state when user logs out.
        case types.AUTH_LOGOUT:
            return initialState;
        default:
            return state;
    }
}