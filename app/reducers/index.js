import { combineReducers } from 'redux';

import { navigation } from './navigation';
import { auth } from './auth';
import { employees } from './employees';

const AppReducer = combineReducers({
	navigation,
	auth,
	employees
});

export default AppReducer;