import { NavigationActions } from 'react-navigation';

import { AppNavigator } from '../navigators/AppNavigator';
import * as types from '../constants/actionTypes';

const loginAction = AppNavigator.router.getActionForPathAndParams('Login');
const initialNavState = { ...AppNavigator.router.getStateForAction(loginAction), isLoading: false }

export function navigation(state = initialNavState, action) {
	let nextState;
	switch (action.type) {
		case types.ROUTE_LOADING_START:
			nextState = { ...state, isLoading: true };
			break;
		case types.ROUTE_LOADING_END:
			nextState = { ...state, isLoading: false };
			break;
		case types.ROUTE_LOGIN_SUCCESS:
			nextState = AppNavigator.router.getStateForAction(
				NavigationActions.navigate({ routeName: 'Home' }),
				state
			);
			break;
		case types.ROUTE_LOGIN_FAIL:
			nextState = { ...state, isLoading: false };
			break;
		case types.ROUTE_HOME:
			nextState = AppNavigator.router.getStateForAction(
				NavigationActions.navigate({ routeName: 'Home' }),
				state
			);
			break;
		case types.ROUTE_TABBED_SCREEN:
			nextState = AppNavigator.router.getStateForAction(
				NavigationActions.navigate({ routeName: 'TabbedScreen' }),
				state
			);
			break;
		case types.ROUTE_PEOPLE:
			nextState = AppNavigator.router.getStateForAction(
				NavigationActions.navigate({ routeName: 'People' }),
				state
			);
			break;
		case types.ROUTE_TEAM:
			nextState = AppNavigator.router.getStateForAction(
				NavigationActions.navigate({ routeName: 'Team' }),
				state
			);
			break;
		case types.ROUTE_LOGOUT:
			nextState = AppNavigator.router.getStateForAction(NavigationActions.navigate({ routeName: 'Login' }));
			break;
		default:
			nextState = AppNavigator.router.getStateForAction(action, state);
			break;
	}

	// Simply return the original `state` if `nextState` is null or undefined.
	return nextState || state;
}