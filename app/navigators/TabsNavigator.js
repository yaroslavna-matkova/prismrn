import React from 'react';
import { BackHandler } from "react-native";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNavigationHelpers, TabNavigator, TabBarBottom } from 'react-navigation';
import { createReduxBoundAddListener } from 'react-navigation-redux-helpers';

import PeopleScreen from '../containers/PeopleScreen';
import TeamScreen from '../containers/TeamScreen';

const TabsNavigator = TabNavigator(
    {
        People: { screen: PeopleScreen },
        Team: { screen: TeamScreen },
    },
    {
        tabBarComponent: TabBarBottom,
        lazy: false,
        tabBarPosition: 'bottom',
        animationEnabled: false,
        backBehavior: 'none',
        tabBarOptions: {
            labelStyle: {
                fontSize: 14
            },
        }
    }
);

export default TabsNavigator;