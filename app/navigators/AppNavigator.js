import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNavigationHelpers, StackNavigator } from 'react-navigation';
import { View } from "react-native";
import { createReduxBoundAddListener } from 'react-navigation-redux-helpers';

import LoginScreen from '../containers/LoginScreen';
import HomeScreen from '../containers/HomeScreen';
import TabsNavigator from './TabsNavigator';
import LogOutButton from '../components/LogOutButton'

export const AppNavigator = StackNavigator(
	{
		Login: { screen: LoginScreen },
		Home: { screen: HomeScreen },
		TabbedScreen: { screen: TabsNavigator }
	},
	{
		navigationOptions: {
			headerRight: <LogOutButton />,
			headerLeft: <View />,
			headerTitleStyle: {
				alignSelf: 'center'
			}
		}
	});

class App extends React.Component {
	static propTypes = {
		dispatch: PropTypes.func.isRequired,
		navigation: PropTypes.object.isRequired,
	};

	render() {
		const { dispatch, navigation } = this.props;
		return (
			<AppNavigator navigation={addNavigationHelpers({
				dispatch,
				state: navigation,
				addListener: () => { createReduxBoundAddListener("root"); },
			})}
			/>
		);
	}
}

const mapStateToProps = state => ({
	navigation: state.navigation,
});

export default connect(mapStateToProps)(App);