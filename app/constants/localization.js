export const strings = {
    loginTitle: 'Log into Prism',
    username: 'Username',
    password: 'Password',
    login: 'Login',
    logout: 'Log out',
    continue: 'Continue',
    homeScreenTitle: 'Welcome',
    peopleScreenTitle: 'People',
    teamScreenTitle: 'My team',
    search: 'Find...',
    teamEmpty: 'There are no people in your Team...',
} //TODO: change to using any lib for this