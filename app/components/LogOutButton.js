import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

import { logout as routeLogout } from '../actions/routeActions';
import { logout as authLogout } from '../actions/authActions';
import { signout } from '../services/prismRequests'
import { strings } from '../constants/localization';

const styles = StyleSheet.create({
    text: {
        marginHorizontal: 15,
    }
});

const LogOutButton = ({ onLogout }) => (
    <TouchableOpacity onPress={() => { signout(); onLogout(); }}>
        <Text style={styles.text}>{strings.logout}</Text>
    </TouchableOpacity>
);

LogOutButton.propTypes = {
    onLogout: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
    onLogout: () => {
        dispatch(routeLogout())
        dispatch(authLogout())
    }
});

export default connect(null, mapDispatchToProps)(LogOutButton);