import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Keyboard } from 'react-native';
import { SearchBar } from 'react-native-elements'

import { strings } from '../constants/localization';

class FilterBar extends React.Component {
    static propTypes = {
        onTextChanged: PropTypes.func.isRequired
    };

    render() {
        const { onTextChanged } = this.props;
        return (
            <SearchBar
                lightTheme
                onChangeText={(text) => onTextChanged(text)}
                onClearText={() => { onTextChanged(''); Keyboard.dismiss(); }}
                icon={{ type: 'simple-line-icon', name: 'magnifier' }}
                clearIcon={{ type: 'simple-line-icon', name: 'close' }}
                placeholder={strings.search} />
        );
    }
}

export default FilterBar;

