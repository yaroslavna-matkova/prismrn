import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { FlatList } from 'react-native';

import TeamItem from './TeamItem';
import { removeFromTeam } from '../actions/employeesActions';

class TeamList extends React.Component {
    static propTypes = {
        team: PropTypes.array.isRequired,
        remove: PropTypes.func.isRequired, 
    };

    render() {
        const { team, remove } = this.props;
        return (
            <FlatList
                numColumns={2}
                data={team}
                keyExtractor={(item, index) => item.Id.toString()}
                renderItem={({ item }) =>
                    <TeamItem
                        id={item.Id}
                        name={item.FirstName + ' ' + item.LastName}
                        onRemoveFromTeam={() => remove(item.Id)}
                    />}
                extraData={team}
            />
        );
    }
}

const mapStateToProps = state => ({
    team: state.employees.list.filter((i) => state.employees.team.indexOf(i.Id) >= 0)
});

const mapDispatchToProps = dispatch => ({
    remove: (id) => dispatch(removeFromTeam(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(TeamList);