import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

import { photoUrl } from '../services/prismRequests';

const styles = StyleSheet.create({
    container: {
        flex: 0.5,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    avatar: {
        width: 100,
        height: 100,
        aspectRatio: 1,
        flex: 1,
        margin: 5,
        borderWidth: 2,
        borderColor: '#8f8f8f',
    },
    name: {
        position: 'absolute',
        right: 5,
        bottom: 5,
        backgroundColor: '#deeeee',
        paddingHorizontal: 5,
        borderWidth: 2,
        borderColor: '#8f8f8f',
    },
    button: {
        position: 'absolute',
        right: 10,
        top: 10,
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
        borderRadius: 25,
    }
});

class TeamItem extends React.PureComponent {
    static propTypes = {
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        onRemoveFromTeam: PropTypes.func.isRequired
    };

    render() {
        const { id, name, onRemoveFromTeam } = this.props;
        return (
            <View style={styles.container}>
                <Image style={styles.avatar} source={{ uri: photoUrl(id) }} />
                <Text style={styles.name} >{name}</Text>
                <TouchableOpacity style={styles.button} onPress={onRemoveFromTeam}>
                    <Icon name={'minus'} size={25} color="#8f8f8f" />
                </TouchableOpacity>
            </View>
        );
    }
}

export default TeamItem;