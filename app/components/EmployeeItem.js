import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { View, Image, Text, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

import { photoUrl } from '../services/prismRequests';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderWidth: 1,
        borderColor: '#8f8f8f'
    },
    avatar: {
        width: 70,
        height: 70,
        marginEnd: 10,
    },
    employeeContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    name: {
        fontSize: 16,
        alignContent: 'flex-start'
    },
    info: {
        fontSize: 12,
        alignContent: 'flex-start'
    },
    button: {
        alignSelf: 'center',
        marginEnd: 10,
    }
});

class EmployeeItem extends React.PureComponent {
    static propTypes = {
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        info: PropTypes.string.isRequired,
        inTeam: PropTypes.bool.isRequired,
        me: PropTypes.bool.isRequired,
        onAddToTeam: PropTypes.func.isRequired,
        onRemoveFromTeam: PropTypes.func.isRequired
    };

    render() {
        const { id, name, info, note, inTeam, me, onAddToTeam, onRemoveFromTeam } = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.employeeContainer}>
                    <Image style={styles.avatar} source={{ uri: photoUrl(id) }} />
                    <View>
                        <Text style={styles.name} >{name}</Text>
                        <Text style={styles.info} >{info}</Text>
                    </View>
                </View>
                {!me && <TouchableOpacity style={styles.button} onPress={inTeam ? onRemoveFromTeam : onAddToTeam}>
                    <Icon name={inTeam ? 'minus' : 'plus'} size={25} color="#8f8f8f" />
                </TouchableOpacity>}
            </View>
        );
    }
}

export default EmployeeItem;