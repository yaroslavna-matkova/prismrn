import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { FlatList } from 'react-native';

import EmployeeItem from './EmployeeItem';
import { addToTeam, removeFromTeam } from '../actions/employeesActions';

class EmployeesList extends React.Component {
    static propTypes = {
        employees: PropTypes.array.isRequired,
        team: PropTypes.array.isRequired,
        me: PropTypes.number.isRequired,
        add: PropTypes.func.isRequired,
        remove: PropTypes.func.isRequired,
    };

    render() {
        const { employees, team, me, add, remove } = this.props;
        return (
            <FlatList
                data={employees}
                keyExtractor={(item, index) => item.Id.toString()}
                renderItem={({ item }) =>
                    <EmployeeItem
                        id={item.Id}
                        name={item.FirstName + ' ' + item.LastName}
                        info={item.Dislocation}
                        inTeam={team.indexOf(item.Id) !== -1}
                        me={item.Id === me}
                        onAddToTeam={() => add(item.Id)}
                        onRemoveFromTeam={() => remove(item.Id)}
                    />}
                extraData={team}
            />
        );
    }
}

const mapStateToProps = state => ({
    employees: state.employees.filteredList,
    team: state.employees.team,
    me: state.auth.user.Id || -1,
});

const mapDispatchToProps = dispatch => ({
    add: (id) => dispatch(addToTeam(id)),
    remove: (id) => dispatch(removeFromTeam(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(EmployeesList);