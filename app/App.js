import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers';

import AppReducer from './reducers';
import App from './navigators/AppNavigator';

const middleware = createReactNavigationReduxMiddleware(
	"root",
	state => state.nav,
);

const store = createStore(
	AppReducer,
	applyMiddleware(middleware),
);

export default class PrismRN extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<App />
			</Provider>
		);
	}
}